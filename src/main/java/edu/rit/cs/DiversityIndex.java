/**
 * DiversityIndex.java
 *   Eric Hartman (ech6581@rit.edu)
 */
package edu.rit.cs;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.FlatMapFunction;
import org.apache.spark.api.java.function.Function;
import org.apache.spark.api.java.function.PairFunction;
import org.apache.spark.sql.*;
import org.apache.spark.sql.types.DataTypes;
import scala.Tuple2;
import scala.Tuple4;
import org.apache.spark.sql.functions;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import static org.apache.spark.sql.functions.sum;

/**
 * DiversityIndex
 *
 * Program to calculate the Diversity Index for US Census Data.
 * This program is heavily influenced by the control flow found
 * in WordCount_Spark (Peizhou Hu, 2019)
 */
public class DiversityIndex {
    // Hardcoded path to our dataset
    public static final String DATASET_PATH = "dataset/cc-est2017-alldata.csv";

    // Debug flag
    public static final boolean DEBUG = false;

    /**
     * Helper method to perform the "partial" Diversity Index calculation for a single racial category
     *   The full diversity index is simply the sum of all of the partial diversity indices.
     *
     * @param total - the total population for the area
     * @param racialCategoryCount - the racial category population for the area
     * @return
     */
    public static Double calculatePartialDiversittyIndex(long total, long racialCategoryCount) {
        // Partial Diversity Index (for a single Racial Category): 1/T^2*(N*(T-N))
        return new Double((double) 1.0 / (double)((double)total*(double)total) * (double)((double)racialCategoryCount*((double)total - (double)racialCategoryCount)));
    }

    /**
     * Helper method to print the [State,DiversityIndex] from the given JavaPairRDD
     * @param rdd
     */
    private static void printStateDiversityIndices(JavaPairRDD<String, Double> rdd) {
        // Construct the output file (States)...
        // Translate From: (State, DiversityIndex)
        // Into output: [State,DiversityIndex]
        List<Tuple2<String, Double>> stateList = rdd.collect();
        for(Tuple2<String, Double> row: stateList) {
            // Get the State
            String state = row._1;
            // Get the Diversity Index
            Double diversityIndex = row._2;

            // Print the line...
            System.out.println("[" + state + "," + diversityIndex + "]");
        }
    }

    /**
     * Helper method to print the [State,County,DiversityIndex] from the given JavaPairRDD
     * @param rdd
     */
    private static void printStateCountyDiversityIndices(JavaPairRDD<Tuple2, Double> rdd) {
        // Construct the output file...
        // Translate from: ((State,County), DiversityIndex)
        // Into output: [State,County,DiversityIndex]
        List<Tuple2<Tuple2, Double>> stateCountyList = rdd.collect();
        for (Tuple2<Tuple2, Double> row: stateCountyList) {
            // Get the State
            String state = row._1._1.toString();
            // Get the County
            String county = row._1._2.toString();
            // Get the Diversity Index
            Double diversityIndex = row._2;

            // Print the line...
            System.out.println("[" + state + "," + county + "," + diversityIndex + "]");
        }
    }

    /**
     * Helper method to write out the contents of a JavaPairRDD to the specified path
     * @param javaPairRDD
     * @param path
     */
    private static void outputJavaPairRDDToPath(JavaPairRDD javaPairRDD, String path) {
        if(DEBUG) {
            deleteDirectory(new File(path));
            javaPairRDD.saveAsTextFile(path);
        }
    }

    /**
     * Helper method to write out the contents of a JavaRDD to the specified path
     * @param javaRDD
     * @param path
     */
    private static void outputJavaRDDToPath(JavaRDD javaRDD, String path) {
        deleteDirectory(new File(path));
        javaRDD.saveAsTextFile(path);
    }

    /**
     * calculateDiversityIndex - Performs all Spark jobs required to
     *   calculate the Diversity Index and generate output results.
     *
     * @param spark - the Spark Session to use
     * @param yearIdentifier - the year identifier (1-10)
     */
    public static void calculateDiversityIndex(SparkSession spark, int yearIdentifier) {
        // Read in the raw dataset, inferring schema...
        Dataset ds = spark.read()
                .option("header", "true")
                .option("delimiter", ",")
                .option("inferSchema", "true")
                .csv(DATASET_PATH);

        // Perform dimensionality reduction to only get the columns we care about...
        ds = ds.select(
                ds.col("STNAME"),
                ds.col("CTYNAME"),
                ds.col("YEAR"),
                ds.col("AGEGRP"),
                ds.col("WA_MALE"),
                ds.col("WA_FEMALE"),
                ds.col("BA_MALE"),
                ds.col("BA_FEMALE"),
                ds.col("IA_MALE"),
                ds.col("IA_FEMALE"),
                ds.col("AA_MALE"),
                ds.col("AA_FEMALE"),
                ds.col("NA_MALE"),
                ds.col("NA_FEMALE"),
                ds.col("TOM_MALE"),
                ds.col("TOM_FEMALE")
        );

        // Assign specific data types to the columns we care about...
        ds = ds.withColumn("STNAME", ds.col("STNAME").cast(DataTypes.StringType))
                .withColumn("CTYNAME", ds.col("CTYNAME").cast(DataTypes.StringType))
                .withColumn("YEAR", ds.col("YEAR").cast(DataTypes.IntegerType))
                .withColumn("AGEGRP", ds.col("AGEGRP").cast(DataTypes.IntegerType))
                .withColumn("WA_MALE", ds.col("WA_MALE").cast(DataTypes.IntegerType))
                .withColumn("WA_FEMALE", ds.col("WA_FEMALE").cast(DataTypes.IntegerType))
                .withColumn("BA_MALE", ds.col("BA_MALE").cast(DataTypes.IntegerType))
                .withColumn("BA_FEMALE", ds.col("BA_FEMALE").cast(DataTypes.IntegerType))
                .withColumn("IA_MALE", ds.col("IA_MALE").cast(DataTypes.IntegerType))
                .withColumn("IA_FEMALE", ds.col("IA_FEMALE").cast(DataTypes.IntegerType))
                .withColumn("AA_MALE", ds.col("AA_MALE").cast(DataTypes.IntegerType))
                .withColumn("AA_FEMALE", ds.col("AA_FEMALE").cast(DataTypes.IntegerType))
                .withColumn("NA_MALE", ds.col("NA_MALE").cast(DataTypes.IntegerType))
                .withColumn("NA_FEMALE", ds.col("NA_FEMALE").cast(DataTypes.IntegerType))
                .withColumn("TOM_MALE", ds.col("TOM_MALE").cast(DataTypes.IntegerType))
                .withColumn("TOM_FEMALE", ds.col("TOM_FEMALE").cast(DataTypes.IntegerType));

        // Use encoders to construct ds1 dataset
        Encoder<USCBRecord> uscbEncoder = Encoders.bean(USCBRecord.class);
        Dataset<USCBRecord> ds2 = ds.as(uscbEncoder).filter(ds.col("AGEGRP").eqNullSafe(0));

        // Filter down to the specified year... (if a non-zero year is specified)
        if(yearIdentifier != 0) {
            ds2 = ds2.filter(ds2.col("YEAR").eqNullSafe((yearIdentifier)));
        }

        // Aggregate the rows in the dataset by sums and group by STNAME and CTYNAME...
        Dataset<Row> ds3 = ds2.groupBy(ds2.col("STNAME"), ds2.col("CTYNAME"))
                .agg(sum("WA_MALE"), sum("WA_FEMALE"),
                        sum("BA_MALE"), sum("BA_FEMALE"),
                        sum("IA_MALE"), sum("IA_FEMALE"),
                        sum("AA_MALE"), sum("AA_FEMALE"),
                        sum("NA_MALE"), sum("NA_FEMALE"),
                        sum("TOM_MALE"), sum("TOM_FEMALE"));
        ds3.show();

        // Show the dataset so far...
        ds2.show();

        // Create Tuples of (State, County, RacialCategory, RacialCategoryCount)
        JavaRDD<Tuple4<String, String, String, Long>> rdd0 = ds3.toJavaRDD().flatMap((FlatMapFunction<Row, Tuple4<String, String, String, Long>>) record -> {
            // Need to return an Iterator over Tuple4s...
            List<Tuple4<String, String, String, Long>> tuple4s = new ArrayList<>();

            // Let's add a Tuple4 for each racial category

            // Racial Categories: WA, BA, IA, AA, NA TOM
            tuple4s.add(new Tuple4(record.getString(0), record.getString(1), "WA", record.getLong(2) + record.getLong(3)));
            tuple4s.add(new Tuple4(record.getString(0), record.getString(1), "BA", record.getLong(4) + record.getLong(5)));
            tuple4s.add(new Tuple4(record.getString(0), record.getString(1), "IA", record.getLong(6) + record.getLong(7)));
            tuple4s.add(new Tuple4(record.getString(0), record.getString(1), "AA", record.getLong(8) + record.getLong(9)));
            tuple4s.add(new Tuple4(record.getString(0), record.getString(1), "NA", record.getLong(10) + record.getLong(11)));
            tuple4s.add(new Tuple4(record.getString(0), record.getString(1), "TOM", record.getLong(12) + record.getLong(13)));

            // Return the iterator to our list of tuple4s
            return tuple4s.iterator();
        });

        // Calculate County Sums: Part 1 - construct pairs by (State, County) -> RacialCategoryCount
        JavaPairRDD<Tuple2, Long> stateCountyPairs = rdd0.mapToPair(s -> new Tuple2<>(new Tuple2<>(s._1(),s._2()), s._4()));

        // Calculate County Sums: Part 2- reduce sums by County
        JavaPairRDD<Tuple2, Long> stateCountyPairsSums = stateCountyPairs.reduceByKey((a, b) -> a+b);
        outputJavaPairRDDToPath(stateCountyPairsSums, "dataset/review-outputs_stateCountySums");

        // Calculate State Sums: Part 1 - construct pairs by State -> RacialCategoryCount
        JavaPairRDD<String, Long> statePairs = rdd0.mapToPair(s -> new Tuple2<>(s._1(), s._4()));
        outputJavaPairRDDToPath(statePairs, "dataset/review-outputs_state");

        // Calculate State Sums: Part 2- reduce sums by State
        JavaPairRDD<String, Long> statePairsSums = statePairs.reduceByKey((a, b) -> a+b);
        outputJavaPairRDDToPath(statePairsSums, "dataset/review-outputs_state_sums");

        // Calculate State RacialCategoryCount Sums: Part 1 - construct pairs by (State, RacialCategory) -> RacialCategoryCount
        JavaPairRDD<Tuple2, Long> stateRacialCategoryCountPairs = rdd0.mapToPair(s -> new Tuple2<>(new Tuple2(s._1(), s._3()), s._4()));
        outputJavaPairRDDToPath(stateRacialCategoryCountPairs, "dataset/review-outputs_stateCountSums");

        // Calculate State RacialCategoryCount Sums: Part 2- reduce sums by (State, RacialCategory)
        JavaPairRDD<Tuple2, Long> stateRacialCategoryCountPairsSums = stateRacialCategoryCountPairs.reduceByKey((a, b) -> a+b);
        outputJavaPairRDDToPath(stateRacialCategoryCountPairsSums, "dataset/review-outputs_stateSums");

        // Calculate (State, N) Reduce dimensionality by mapping ((State, RacialCategory), RacialCategoryStateSum) -> (State, RacialCategoryStateSum)
        JavaPairRDD<String, Long> stateCategoryCountPairs = stateRacialCategoryCountPairsSums.mapToPair(new PairFunction<Tuple2<Tuple2, Long>, String, Long>() {
            @Override
            public Tuple2<String, Long> call(Tuple2<Tuple2, Long> tuple) throws Exception {
                return new Tuple2<>(tuple._1._1.toString(), tuple._2);
            }
        });

        // Calculate (State, T)
        JavaPairRDD<String, Long> stateCategoryCountPairsSums = stateCategoryCountPairs.reduceByKey((a,b) -> a+b);

        // Calculate Diversity Index per County
        // SUM ( 1/T^2*(N*(T-N)) )
        // Need: ((State, County), T, N)

        // Use join to construct ((State, County), (T, N))
        JavaPairRDD<Tuple2, Tuple2<Long, Long>> stateCountyJoined = stateCountyPairsSums.join(stateCountyPairs);
        outputJavaPairRDDToPath(stateCountyJoined, "dataset/review-outputs_join");

        // Map: (State, County) -> 1/T^2*(N*(T-N))
        JavaPairRDD<Tuple2, Double> stateCountyDiversityIndices = stateCountyJoined.mapToPair(s ->  new Tuple2(s._1,
                calculatePartialDiversittyIndex(s._2._1, s._2._2)));
        outputJavaPairRDDToPath(stateCountyDiversityIndices, "dataset/review-outputs_diversity");

        // Sum: (State, County) -> Diversity Indexe
        JavaPairRDD<Tuple2, Double> stateCountyPairsDiversityIndices = stateCountyDiversityIndices.reduceByKey((a, b) -> a+b);

        // Sort the County Diversity Indices
        StateCountyDiversityIndexComparator comparator = new StateCountyDiversityIndexComparator();
        JavaPairRDD<Tuple2, Double> stateCountyPairsDiversityIndicesSorted = stateCountyPairsDiversityIndices.sortByKey(comparator);
        stateCountyPairsDiversityIndicesSorted = stateCountyPairsDiversityIndicesSorted.repartition(1);
        outputJavaPairRDDToPath(stateCountyPairsDiversityIndicesSorted, "dataset/review-outputs-final-states-and-counties");

        // Convert the <Tuple2, Double> into an Dataset<USCBTriple>
        JavaRDD<USCBTriple> solution = stateCountyPairsDiversityIndicesSorted.map(new Function<Tuple2<Tuple2, Double>, USCBTriple>() {
            @Override
            public USCBTriple call(Tuple2<Tuple2, Double> tuple2DoubleTuple2) throws Exception {
                return new USCBTriple(tuple2DoubleTuple2._1._1.toString(), tuple2DoubleTuple2._1._2.toString(), tuple2DoubleTuple2._2);
            }
        });
        Dataset<USCBTriple> solutionDataset =  spark.createDataset(solution.rdd(), Encoders.bean(USCBTriple.class));
        solutionDataset.show();
        outputJavaRDDToPath(solution, "dataset/DiversityIndex");

        //================================================================
        //
        //  The remainder of this program constructs the State-level
        //  Diversity Indices, which are no longer required.
        //
        //================================================================

        // Calculate Diversity Index per State
        // Need (State, RacialCategory, T, N)

        // Use join to construct ((State, RacialCategory), (T, N))
        JavaPairRDD<String, Tuple2<Long, Long>> stateJoined = stateCategoryCountPairsSums.join(stateCategoryCountPairs);
        outputJavaPairRDDToPath(stateJoined, "dataset/review-outputs_joinState");

        // Map: (State) -> 1/T^2*(N*(T-N))
        JavaPairRDD<String, Double> stateDiversityIndices = stateJoined.mapToPair(s -> new Tuple2(s._1,
                calculatePartialDiversittyIndex(s._2._1, s._2._2)));
        outputJavaPairRDDToPath(stateDiversityIndices, "dataset/review-outputs_diversity_state");

        // Sum: (State) -> Diversity Indices
        JavaPairRDD<String, Double> statePairsDiversityIndices = stateDiversityIndices.reduceByKey((a, b) -> a+b);

        // Sort the State Diversity Indices
        JavaPairRDD<String, Double> statePairsDiversityIndicesSorted = statePairsDiversityIndices.sortByKey(true);
        statePairsDiversityIndicesSorted = statePairsDiversityIndicesSorted.repartition(1);
        outputJavaPairRDDToPath(statePairsDiversityIndicesSorted, "dataset/review-outputs-final-states");

        // Print the results to the console output...
        printStateDiversityIndices(statePairsDiversityIndicesSorted);
        printStateCountyDiversityIndices(stateCountyPairsDiversityIndicesSorted);
    }

    /**
     * Helper method used to delete the contents of the given directory.
     *   Borrowed from WordCount_Spark.java (Peizhou Hu, 2019)
     *
     * @param directoryToBeDeleted - directory to be deleted.
     * @return - success result
     */
    public static boolean deleteDirectory(File directoryToBeDeleted) {
        File[] allContents = directoryToBeDeleted.listFiles();
        if (allContents != null)
            for (File file : allContents)
                deleteDirectory(file);
        return directoryToBeDeleted.delete();
    }

    /**
     * Main entry point for DiversityIndex Apache Spark program.
     *
     * @param args
     * @throws Exception
     */
    public static void main(String[] args) throws Exception {
        // Create the Spark Configuration
        SparkConf sparkConf = new SparkConf();
        sparkConf.set("spark.master", "local[8]");

        // Set the application name
        sparkConf.setAppName("Diversity Index");

        // Create the Spark Session
        SparkSession spark = SparkSession.builder().config(sparkConf).getOrCreate();

        // Create the Spark Context
        JavaSparkContext jsc = new JavaSparkContext(spark.sparkContext());

        // Run Spark Job(s) to Calculate the Diversity Index...
        calculateDiversityIndex(spark, 0);

        // Close the Spark Context
        jsc.close();

        // Close the Spark Session
        spark.close();
    }
}
