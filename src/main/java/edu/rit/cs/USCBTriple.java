/**
 * USCBTriple.java
 *   Eric Hartman (ech6581@rit.edu)
 */

package edu.rit.cs;

import java.io.Serializable;

/**
 * POJO representing a triple (State, County, DiversityIndex) from our USCB dataset
 * This class is used to produce the final output from DiversityIndex.java
 */
public class USCBTriple implements Serializable {
    private String State;
    private String County;
    private double DiversityIndex;

    public String getState() {
        return State;
    }

    public void setState(String state) {
        State = state;
    }

    public String getCounty() {
        return County;
    }

    public void setCounty(String county) {
        County = county;
    }

    public double getDiversityIndex() { return DiversityIndex; }

    public void setDiversityIndex(double diversityIndex) { DiversityIndex = diversityIndex; }

    public USCBTriple() {
        // Do nothing constructor
    }

    public USCBTriple(String State, String County, double DiversityIndex) {
        this.State = State;
        this.County = County;
        this.DiversityIndex = DiversityIndex;
    }

    @Override
    public String toString() {
        return "[" +  State + "," + County + "," + DiversityIndex + "]";
    }
}
