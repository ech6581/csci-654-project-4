/**
 * StateCountyDiversityIndexComparator.java
 *   Eric Hartman (ech6581@rit.edu)
 */

package edu.rit.cs;

import scala.Tuple2;
import java.io.Serializable;
import java.util.Comparator;

/**
 * StateCountyDiversityIndexComparator
 *   Helper class to abstract the sort comparator needed for sorting a JavaPairRDD by a
 *   Tuple2<String,String> key representing the "State" and "County" in the USCB dataset.
 *
 *   Must implement Serializable in order to support deployment in Apache Spark worker nodes
 */
public class StateCountyDiversityIndexComparator implements Comparator<Tuple2>, Serializable {
    @Override
    /**
     * Performs ascending sort by State, then County
     */
    public int compare(Tuple2 t1, Tuple2 t2) {
        // Get convenient strings for states/counties comparisons
        String state1 = t1._1.toString();
        String state2 = t2._1.toString();
        String county1 = t1._2.toString();
        String county2 = t2._2.toString();

        // Calculate our comparison based on strings...
        int stateComparison = state1.compareTo(state2);

        if(stateComparison == 0) {
            return county1.compareTo(county2);
        } else {
            return stateComparison;
        }
    }
}
