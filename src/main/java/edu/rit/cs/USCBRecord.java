/**
 * USCBRecord.java
 *   Eric Hartman (ech6581@rit.edu)
 */

package edu.rit.cs;

import java.io.Serializable;

/**
 * POJO representing a US Census Bureua record from our dataset
 */
public class USCBRecord implements Serializable {
    // Private members
    private String STNAME;
    private String CTYNAME;
    private int YEAR;
    private int AGEGRP;
    private int WA_MALE;
    private int WA_FEMALE;
    private int BA_MALE;
    private int BA_FEMALE;
    private int IA_MALE;
    private int IA_FEMALE;
    private int AA_MALE;
    private int AA_FEMALE;
    private int NA_MALE;
    private int NA_FEMALE;
    private int TOM_MALE;
    private int TOM_FEMALE;

    // Getters and Setters

    public String getSTNAME() {
        return STNAME;
    }

    public void setSTNAME(String STNAME) {
        this.STNAME = STNAME;
    }

    public String getCTYNAME() {
        return CTYNAME;
    }

    public void setCTYNAME(String CTYNAME) {
        this.CTYNAME = CTYNAME;
    }

    public Integer getYEAR() {
        return YEAR;
    }

    public void setYEAR(Integer YEAR) {
        this.YEAR = YEAR;
    }

    public Integer getAGEGRP() {
        return AGEGRP;
    }

    public void setAGEGRP(Integer AGEGRP) {
        this.AGEGRP = AGEGRP;
    }

    public Integer getWA_MALE() {
        return WA_MALE;
    }

    public void setWA_MALE(Integer WA_MALE) {
        this.WA_MALE = WA_MALE;
    }

    public Integer getWA_FEMALE() {
        return WA_FEMALE;
    }

    public void setWA_FEMALE(Integer WA_FEMALE) {
        this.WA_FEMALE = WA_FEMALE;
    }

    public Integer getBA_MALE() {
        return BA_MALE;
    }

    public void setBA_MALE(Integer BA_MALE) {
        this.BA_MALE = BA_MALE;
    }

    public Integer getBA_FEMALE() {
        return BA_FEMALE;
    }

    public void setBA_FEMALE(Integer BA_FEMALE) {
        this.BA_FEMALE = BA_FEMALE;
    }

    public Integer getIA_MALE() {
        return IA_MALE;
    }

    public void setIA_MALE(Integer IA_MALE) {
        this.IA_MALE = IA_MALE;
    }

    public Integer getIA_FEMALE() {
        return IA_FEMALE;
    }

    public void setIA_FEMALE(Integer IA_FEMALE) {
        this.IA_FEMALE = IA_FEMALE;
    }

    public Integer getAA_MALE() {
        return AA_MALE;
    }

    public void setAA_MALE(Integer AA_MALE) {
        this.AA_MALE = AA_MALE;
    }

    public Integer getAA_FEMALE() {
        return AA_FEMALE;
    }

    public void setAA_FEMALE(Integer AA_FEMALE) {
        this.AA_FEMALE = AA_FEMALE;
    }

    public Integer getNA_MALE() {
        return NA_MALE;
    }

    public void setNA_MALE(Integer NA_MALE) {
        this.NA_MALE = NA_MALE;
    }

    public Integer getNA_FEMALE() {
        return NA_FEMALE;
    }

    public void setNA_FEMALE(Integer NA_FEMALE) {
        this.NA_FEMALE = NA_FEMALE;
    }

    public Integer getTOM_MALE() {
        return TOM_MALE;
    }

    public void setTOM_MALE(Integer TOM_MALE) {
        this.TOM_MALE = TOM_MALE;
    }

    public Integer getTOM_FEMALE() {
        return TOM_FEMALE;
    }

    public void setTOM_FEMALE(Integer TOM_FEMALE) {
        this.TOM_FEMALE = TOM_FEMALE;
    }

    /**
     * Empty POJO constructor needed to exist by Apache Spark depeneding on operations performed
     * by worker jobs.
     */
    public USCBRecord() {
        // Do nothing constructor
    }

    /**
     * Instantiates a POJO representing a USCB "Record" from the census dataset
     * @param reviewLine - a line of text from the census dataset
     */
    public USCBRecord(String reviewLine) {
        // This regex parsing block of code is borrowed from AmazonFineFoodReview.java (Peizhou Hu, 2019)
        String otherThanQuote = " [^\"] ";
        String quotedString = String.format(" \" %s* \" ", otherThanQuote);
        String regex = String.format("(?x) "+ // enable comments, ignore white spaces
                        ",                         "+ // match a comma
                        "(?=                       "+ // start positive look ahead
                        "  (?:                     "+ //   start non-capturing group 1
                        "    %s*                   "+ //     match 'otherThanQuote' zero or more times
                        "    %s                    "+ //     match 'quotedString'
                        "  )*                      "+ //   end group 1 and repeat it zero or more times
                        "  %s*                     "+ //   match 'otherThanQuote'
                        "  $                       "+ // match the end of the string
                        ")                         ", // stop positive look ahead
                otherThanQuote, quotedString, otherThanQuote);

        String [] data = reviewLine.split(regex, -1);

        try {
            this.STNAME = data[0];
            this.CTYNAME = data[1];
            this.YEAR = Integer.valueOf(data[2]);
            this.AGEGRP = Integer.valueOf(data[3]);
            this.WA_MALE = Integer.valueOf(data[4]);
            this.WA_FEMALE = Integer.valueOf(data[5]);
            this.BA_MALE = Integer.valueOf(data[6]);
            this.BA_FEMALE = Integer.valueOf(data[7]);
            this.IA_MALE = Integer.valueOf(data[8]);
            this.IA_FEMALE = Integer.valueOf(data[9]);
            this.AA_MALE = Integer.valueOf(data[10]);
            this.AA_FEMALE = Integer.valueOf(data[11]);
            this.NA_MALE = Integer.valueOf(data[12]);
            this.NA_FEMALE = Integer.valueOf(data[13]);
            this.TOM_MALE = Integer.valueOf(data[14]);
            this.TOM_FEMALE = Integer.valueOf(data[15]);
        }catch(Exception e){
            System.err.println(e.toString());
        }
    }
}
